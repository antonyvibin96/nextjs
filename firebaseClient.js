import firebase from "firebase";

const FIREBASE_CONFIG={
    apiKey: "AIzaSyCNPfKitdYxmT0BmIEJJz697-DYi-yZSZg",
    authDomain: "erp-nextjs.firebaseapp.com",
    projectId: "erp-nextjs",
    storageBucket: "erp-nextjs.appspot.com",
    messagingSenderId: "1067540823721",
    appId: "1:1067540823721:web:f94a9a98b38222f6d233b8",
    measurementId: "G-ET7HJBSQPY"
}

export default function firebaseClient() {
    if (!firebase.apps.length) {
      firebase.initializeApp(FIREBASE_CONFIG);
    }
  }