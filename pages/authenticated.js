import React from "react";
import nookies from "nookies";
import { verifyIdToken } from "../firebaseAdmin";
import firebaseClient from "../firebaseClient";
import firebase from "firebase/app";
import { Box, Flex, Text, Heading, Button } from "@chakra-ui/react";
import VerticalBar from "../component/VerticalBar";
import Line from "../component/Line";
import MultiAxisLine from "../component/MultiAxisLine";
import Dynamic from "../component/Dynamic";
import PieChart from "../component/Pie";
function Authenticated({ session }) {
  firebaseClient();
  if (session) {
    return (
      <Flex>
        <Box my={12} mx="auto" width="500px">
            <Button
              width="100%"
              variant="solid"
              variantColor="red"
              onClick={async () => {
                await firebase.auth().signOut();
                window.location.href = "/";
              }}
            >
              Sign out
            </Button>
          </Box>
        <Box w={500} p={4} my={12} mx="auto">
          <VerticalBar/>
        </Box>
        <Box w={500} p={4} my={12} mx="auto">
          <Line/>
        </Box>
        <Box w={500} p={4} my={12} mx="auto">
          <MultiAxisLine/>
        </Box>
        <Box w={500} p={4} my={12} mx="auto">
          <Dynamic/>
        </Box>
        <Box w={500} p={4} my={12} mx="auto">
          <PieChart/>
        </Box>
      </Flex>
    );
  } else {
    return (
      <Box>
        <Text>loading</Text>
      </Box>
    );
  }
}

export async function getServerSideProps(context) {
  try {
    const cookies = nookies.get(context);
    const token = await verifyIdToken(cookies.token);
    const { uid, email } = token;
    return {
      props: { session: `Your email is ${email} and your UID is ${uid}.` },
    };
  } catch (err) {
    context.res.writeHead(302, { Location: "/login" });
    context.res.end();
    return { props: {} };
  }
}
export default Authenticated;
